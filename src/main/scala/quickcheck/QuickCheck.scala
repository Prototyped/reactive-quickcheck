package quickcheck

import common._

import org.scalacheck._
import Arbitrary._
import Gen._
import Prop._
import scala.math.min

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {

  property("MinOfInsertingIntIntoEmptyHeapYieldsInt") = forAll { a: Int =>
    val h = insert(a, empty)
    findMin(h) == a
  }

  property("MinOfInsertingMinIntoHeapIsMinItself") = forAll { (heap: H) =>
    val minimum = if (isEmpty(heap)) 0 else findMin(heap)
    findMin(insert(minimum, heap)) == minimum
  }

  property("MinOfHeapWithTwoIntsIsMinOfTheTwo") = forAll { (a: Int, b: Int) =>
    val heap = insert(b, insert(a, empty))
    findMin(heap) == min(a, b)
  }

  property("deletingMinElementOfSingletonHeapGivesEmptyHeap") =
    forAll { (a: Int) =>
      val heap = insert(a, empty)
      deleteMin(heap) == empty
    }

  def heapMinList(heap: H, minList: List[A]): List[A] =
    if (isEmpty(heap)) minList.reverse
    else {
      val head = findMin(heap)
      heapMinList(deleteMin(heap), head :: minList)
    }

  def heapMinOrder(heap: H, cond: Boolean, previous: A): Boolean =
    if (isEmpty(heap) || !cond) cond
    else {
      val head = findMin(heap)
      heapMinOrder(deleteMin(heap), cond && previous <= head, head)
    }

  property("successiveMinsFromHeapAreInOrder") = forAll { (heap: H) =>
    heapMinOrder(heap, true, Int.MinValue)
  }

  property("MinOfTwoHeapMeldedIsMinOfMins") = forAll { (heap1: H, heap2: H) =>
    val heap1Min = findMin(heap1)
    val heap2Min = findMin(heap2)
    val heapMeldMin = findMin(meld(heap1, heap2))
    heapMeldMin == min(heap1Min, heap2Min)
  }

  property("HeapMinListIsOrderedVersionOfInsertedList") = forAll { (list: List[Int]) =>
    def listToHeap(l: List[Int], h: H): H = l match {
      case Nil => h
      case x :: xs => listToHeap(xs, insert(x, h))
    }
    val heap = listToHeap(list, empty)
    val minOrderList = heapMinList(heap, Nil)
    minOrderList == (list sortWith (_ < _))
  }

  lazy val genHeap: Gen[H] = for {
    value <- arbitrary[Int]
    heap <- oneOf(empty, genHeap)
  } yield insert(value, heap)

  lazy val genList: Gen[List[Int]] = for {
    value <- arbitrary[Int]
    list <- oneOf(Nil, genList)
  } yield value :: list

  implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)
}
